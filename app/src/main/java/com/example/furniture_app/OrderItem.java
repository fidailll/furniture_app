package com.example.furniture_app;

import com.example.furniture_app.model.OrderModel;

//Интерфейс для OrderAdapter
public interface OrderItem {
    void call(int id, OrderModel model);
}
