package com.example.furniture_app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;

public class PaymentActivity extends AppCompatActivity {

    FirebaseFirestore db;
    String idKey;
    String idKeyOrder;

    int sum;

    TextView  textSum;

    Button btPay;
    Button btPaymentBack;

    EditText inputNumberCart;
    EditText inputMonth;
    EditText inputYear;
    EditText inputCVV;
    EditText inputCartHolderName;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getIntent().getExtras();

        assert arguments != null;

        idKey = arguments.get("idKey").toString();

        idKeyOrder = arguments.get("idKeyOrder").toString();

        sum = (int) arguments.get("sum");

        setContentView(R.layout.activity_payment);

        db = FirebaseFirestore.getInstance();

        textSum = findViewById(R.id.textSum);

        btPay = findViewById(R.id.btPay);
        btPaymentBack = findViewById(R.id.btPaymentBack);

        inputNumberCart = findViewById(R.id.inputNumberCart);
        inputMonth = findViewById(R.id.inputMonth);
        inputYear = findViewById(R.id.inputYear);
        inputCVV = findViewById(R.id.inputCVV);
        inputCartHolderName = findViewById(R.id.inputCartHolderName);

        textSum.setText(priceToString(sum));

        onClickBack();

        onClickPay();
    }


    private void onClickPay(){
        btPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (inputNumberCart.length() != 16 ) {
                    Toast.makeText(PaymentActivity.this, "Введите номер карты!",
                            Toast.LENGTH_SHORT).show();
                    inputNumberCart.setError("Введите номер карты!");
                } else if ( inputMonth.length() != 2) {
                    Toast.makeText(PaymentActivity.this, "Введите месяц!",
                            Toast.LENGTH_SHORT).show();
                            inputMonth.setError("Введите месяц");

                } else if ( inputYear.length() != 2) {
                    Toast.makeText(PaymentActivity.this, "Введите год!",
                            Toast.LENGTH_SHORT).show();
                    inputYear.setError("Введите год!");

                }else if ( inputCVV.length() != 3) {
                    Toast.makeText(PaymentActivity.this, "Введите CVV!",
                            Toast.LENGTH_SHORT).show();
                    inputCVV.setError("Введите CVV!");

                }else if ( inputCartHolderName.length() == 0) {
                    Toast.makeText(PaymentActivity.this, "Введите имя владельца карты!",
                            Toast.LENGTH_SHORT).show();
                    inputCartHolderName.setError("Введите имя владельца карты");

                } else {
                    onPay();
                }
            }
        });
    }

    private void onPay(){
        db.collection("users")
                .document(idKey)
                .collection("orders")
                .document(idKeyOrder).update("isPaid", true).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
//                        Toast.makeText(PaymentActivity.this, "Заказ оплачен!!!", Toast.LENGTH_SHORT).show();
                        db.collection("users")
                                .document(idKey)
                                .collection("orders")
                                .document(idKeyOrder).update("status", "Заказ оплачен!").addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        Toast.makeText(PaymentActivity.this, "Заказ оплачен!!!", Toast.LENGTH_SHORT).show();

                                        Intent data = new Intent();
                                        data.putExtra(HistoryOfOrdersActivity.ACCESS_MESSAGE, "true");
                                        setResult(RESULT_OK, data);
                                        finish();
                                    }

                                });
                    }

                });






    }

    private void onClickBack(){
        btPaymentBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    String priceToString(int price){
        int ruble = price / 100;
        int kopeck = price % 100;
        if (kopeck <= 9){
            return String.valueOf(ruble) + ".0" + String.valueOf(kopeck) + " Руб.";
        }
//        else if (kopeck % 10 == 0 || kopeck % 100 == 0){
//            return String.valueOf(ruble) + "." + String.valueOf(kopeck) + "0";
//        }

        return String.valueOf(ruble) + "." + String.valueOf(kopeck) + " Руб.";
    }
}