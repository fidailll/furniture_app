package com.example.furniture_app.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.furniture_app.OrderItem;
import com.example.furniture_app.R;
import com.example.furniture_app.model.OrderModel;

import java.util.List;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.OrderViewHolder> {

    Context context;

    List<OrderModel> orders;

    OrderItem orderItem;


    public OrderAdapter(Context context, List<OrderModel> orders, OrderItem orderItem) {
        this.context = context;
        this.orders = orders;
        this.orderItem = orderItem;
    }


    //Здесь указывается дизайн и с какими элементами будет происходить взаимодействие
    @Override
    public OrderAdapter.OrderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View items = LayoutInflater.from(context).inflate(R.layout.order_item, parent, false);
        return new OrderAdapter.OrderViewHolder(items);
    }

    ///Создается обьект на основе вложеного класса
    @Override
    public void onBindViewHolder(OrderAdapter.OrderViewHolder holder, @SuppressLint("RecyclerView") int position) {
        holder.textId.setText(String.valueOf(orders.get(position).getId()));
        holder.textDate.setText(orders.get(position).getDate());
        holder.textAddress.setText(orders.get(position).getAddress());
        holder.textSum.setText(priceToString(orders.get(position).getSum()));
        holder.textStatus.setText(orders.get(position).getStatus());



        holder.itemView
                .setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Log.d("OrderAdapter", "OnClick");
                                            orderItem.call(orders.get(position).getId(), orders.get(position));
                                        }
                                    }

                );

    }


    //преобразует цену в строкое представление
    String priceToString(int price) {
        int ruble = price / 100;
        int kopeck = price % 100;
        if (kopeck <= 9) {
            return String.valueOf(ruble) + ".0" + String.valueOf(kopeck) + " Руб.";
        }
//        else if (kopeck % 10 == 0 || kopeck % 100 == 0){
//            return String.valueOf(ruble) + "." + String.valueOf(kopeck) + "0";
//        }

        return String.valueOf(ruble) + "." + String.valueOf(kopeck) + " Руб.";
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    public static class OrderViewHolder extends RecyclerView.ViewHolder {

        TextView textId;
        TextView textDate;
        TextView textAddress;
        TextView textSum;
        TextView textStatus;

        OrderViewHolder(View itemView) {
            super(itemView);
            textId = itemView.findViewById(R.id.textId);
            textDate = itemView.findViewById(R.id.textDate);
            textAddress = itemView.findViewById(R.id.textAddress);
            textSum = itemView.findViewById(R.id.textSum);
            textStatus = itemView.findViewById(R.id.textStatus);
        }
    }
}
