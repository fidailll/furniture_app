package com.example.furniture_app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.furniture_app.R;
import com.example.furniture_app.model.ProductModel;
import com.example.furniture_app.model.UserProductModel;
import com.squareup.picasso.Picasso;

import java.util.List;

public class HistoryOfOrdersAdapter extends RecyclerView.Adapter<HistoryOfOrdersAdapter. HistoryOfOrdersViewHolder>{
    //Страница на которую все будет выводится
    Context context;
    List<UserProductModel> products;



    public HistoryOfOrdersAdapter(Context context, List<UserProductModel> products) {
        this.context = context;
        this.products = products;
    }

    @NonNull
    @Override
    public HistoryOfOrdersAdapter.HistoryOfOrdersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View categoryItems = LayoutInflater.from(context).inflate(R.layout.history_of_order_item, parent, false);
        return new HistoryOfOrdersAdapter.HistoryOfOrdersViewHolder(categoryItems);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryOfOrdersAdapter.HistoryOfOrdersViewHolder holder, int position) {
        boolean isImage = products.get(position).getSrcImage() == null;

        if(isImage){
            Picasso.get().load(R.drawable.image_no_image).into(holder.imageProduct);
        } else {
            Picasso.get().load( products.get(position).getSrcImage()).into(holder.imageProduct);
        }

        int totalAmount = products.get(position).getPrice() * products.get(position).getCount();
        holder.textCartPrice.setText(priceToString(totalAmount) );

        holder.textNameCart.setText(products.get(position).getTitle());
        holder.textCartPrice.setText(priceToString(totalAmount) );
        holder.textCartQuantity.setText(String.valueOf(products.get(position).getCount()));
    }
    //преобразует цену в строкое представление
    String priceToString(int price){
        int ruble = price / 100;
        int kopeck = price % 100;
        if (kopeck <= 9){
            return String.valueOf(ruble) + ".0" + String.valueOf(kopeck) + " Руб.";
        }
//        else if (kopeck % 10 == 0 || kopeck % 100 == 0){
//            return String.valueOf(ruble) + "." + String.valueOf(kopeck) + "0";
//        }

        return String.valueOf(ruble) + "." + String.valueOf(kopeck) + " Руб.";
    }
    @Override
    public int getItemCount() {
        return products.size();
    }

    public static final class HistoryOfOrdersViewHolder extends RecyclerView.ViewHolder {
        //        TextView categoryTitle;
        ImageView imageProduct;
        TextView textNameCart;
        TextView textCartPrice;
        TextView textCartQuantity;
        public HistoryOfOrdersViewHolder(@NonNull View itemView) {
            super(itemView);

            imageProduct =  itemView.findViewById(R.id.imageHistoryOfOrdersItem);
            textNameCart =  itemView.findViewById(R.id.nameHistoryOfOrdersItemText);
            textCartPrice =  itemView.findViewById(R.id.priceHistoryOfOrdersItemText);
            textCartQuantity =  itemView.findViewById(R.id.textHistoryOfOrdersItem);
        }
    }
}
