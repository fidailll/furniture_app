package com.example.furniture_app.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.furniture_app.CartItem;
import com.example.furniture_app.LogOut;
import com.example.furniture_app.R;
import com.example.furniture_app.model.ProductModel;
import com.example.furniture_app.model.UserProductModel;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.CartViewHolder>{
    //Страница на которую все будет выводится
    Context context;
    List<UserProductModel> products;

    private CartItem cartItem;



    public CartAdapter(Context context, List<UserProductModel> products, CartItem cartItem) {
        this.context = context;
        this.products = products;
        this.cartItem = cartItem;
    }

    //Здесь указывается дизайн и с какими элементами будет происходить взаимодействие
    @NonNull
    @Override
    public CartAdapter.CartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View categoryItems = LayoutInflater.from(context).inflate(R.layout.cart_item, parent, false);
        return new CartAdapter.CartViewHolder(categoryItems);
    }

    ///Создается обьект на основе вложеного класса
    @Override
    public void onBindViewHolder(@NonNull CartAdapter.CartViewHolder holder, @SuppressLint("RecyclerView") int position) {

        boolean isImage = products.get(position).getSrcImage() == null;

        if(isImage){
            Picasso.get().load(R.drawable.image_no_image).into(holder.imageProduct);
        } else {
            Picasso.get().load( products.get(position).getSrcImage()).into(holder.imageProduct);
        }

        holder.textNameCart.setText(products.get(position).getTitle());
        int totalAmount = products.get(position).getPrice() * products.get(position).getCount();
                holder.textCartPrice.setText(priceToString(totalAmount) );

        holder.textCount.setText(String.valueOf(products.get(position).getCount()));

        //Удалить
        holder.btDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteItemByPosition(position);
            }
        });

        //Увеличить на 1
        holder.btAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addCount(position);
            }
        });

        //Уменьшить на 1
        holder.btMinimize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                minimize(position);
            }
        });
    }

    public void deleteItemByPosition(int position){
        cartItem.delete(products.get(position));
        products.remove(position);
        notifyDataSetChanged();
    }

    public void addCount(int position){
        products.get(position).setCount(products.get(position).getCount() + 1);
        cartItem.add(products.get(position));
        notifyDataSetChanged();
    }
    public void minimize(int position){
        if(products.get(position).getCount() > 1){
            products.get(position).setCount(products.get(position).getCount() - 1);
        }

        cartItem.minimize(products.get(position));
        notifyDataSetChanged();
    }

    //преобразует цену в строкое представление
    String priceToString(int price){
        int ruble = price / 100;
        int kopeck = price % 100;
        if (kopeck <= 9){
            return String.valueOf(ruble) + ".0" + String.valueOf(kopeck) + " Руб.";
        }
//        else if (kopeck % 10 == 0 || kopeck % 100 == 0){
//            return String.valueOf(ruble) + "." + String.valueOf(kopeck) + "0";
//        }

        return String.valueOf(ruble) + "." + String.valueOf(kopeck) + " Руб.";
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public static final class CartViewHolder extends RecyclerView.ViewHolder {

        //        TextView categoryTitle;
        ImageView imageProduct;
        TextView textNameCart;
        TextView textCartPrice;
        TextView textCount;

        Button btDelete;
        Button btAdd;
        Button btMinimize;
        public CartViewHolder(@NonNull View itemView) {
            super(itemView);

            imageProduct =  itemView.findViewById(R.id.productCartImage);
            textNameCart =  itemView.findViewById(R.id.nameCartText);
            textCartPrice =  itemView.findViewById(R.id.priceCartText);
            textCount =  itemView.findViewById(R.id.textCount);
            btDelete = itemView.findViewById(R.id.buttonCartDelete);
            btAdd = itemView.findViewById(R.id.buttonCartAdd);
            btMinimize = itemView.findViewById(R.id.buttonCartMinimize);



        }
    }

}
