package com.example.furniture_app.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.furniture_app.R;
import com.example.furniture_app.model.CategoryModel;
import com.example.furniture_app.CategoryProductActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> {
    //Страница на которую все будет выводится
    static Context context;
    List<CategoryModel> categories;

    public CategoryAdapter(Context context, List<CategoryModel> categories) {
        this.context = context;
        this.categories = categories;
    }

    //Здесь указывается дизайн и с какими элементами будет происходить взаимодействие
    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View categoryItems = LayoutInflater.from(context).inflate(R.layout.category_item, parent, false);

        return new CategoryViewHolder(categoryItems);


    }

    ///Создается обьект на основе вложеного класса
    @Override
    public void onBindViewHolder(@NonNull CategoryAdapter.CategoryViewHolder holder, int position) {

//        Log.v(TAG, categories.get(position).getTitle());
//        holder.imageCategory.setImageResource( categories.get(position).getSrcPhoto());

        boolean isImage = categories.get(position).getSrcPhoto() == null;

        if(isImage){
            Picasso.get().load(R.drawable.image_no_image).into(holder.imageCategory);
        } else {
            Picasso.get().load( categories.get(position).getSrcPhoto()).into(holder.imageCategory);
        }

        holder.imageCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CategoryProductActivity.class);
               intent.putExtra(CategoryModel.class.getName(), categories.get(holder.getAdapterPosition()));
                context.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public static final class CategoryViewHolder extends RecyclerView.ViewHolder  {

//        TextView categoryTitle;
        ImageView imageCategory;
        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);

            imageCategory = itemView.findViewById(R.id.imageCategory);


        }

    }

}

