package com.example.furniture_app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.furniture_app.R;
import com.example.furniture_app.model.ProductModel;
import com.squareup.picasso.Picasso;

import java.util.List;

//Нужен для GridView
public class ProductAdapterGrid extends ArrayAdapter<ProductModel> {

    Context context;
    List<ProductModel> categories;
    // Конструктор

    public ProductAdapterGrid(Context context, int textViewResourceId,  List<ProductModel> categories) {
        super(context,textViewResourceId);

        this.categories = categories;
        this.context = context;
    }

    public void ImageTextAdapter(Context c) {
        context = c;
    }

    public int getCount() {
        return categories.size();
    }

    public ProductModel getItem(int position) {

        return categories.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        // TODO Auto-generated method stub

        View grid;

        if (convertView == null) {
            grid = new View(context);
            //LayoutInflater inflater = getLayoutInflater();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
            grid = inflater.inflate(R.layout.product_item, parent, false);
//            int height = parent.getHeight();
//            if (height > 0) {
//                ViewGroup.LayoutParams layoutParams =  convertView.getLayoutParams();
//                layoutParams.height = (int) (height / 2);
//            } // for the 1st item parent.getHeight() is not calculated yet
        } else {
            grid = (View) convertView;
        }

        ImageView imageProduct = (ImageView) grid.findViewById(R.id.imageProduct);
        TextView productTitleText = (TextView) grid.findViewById(R.id.textProductTitle);
        TextView productPriceText = (TextView) grid.findViewById(R.id.textProductPrice);
        TextView poductGradeText = (TextView) grid.findViewById(R.id.textProductGrade);

        boolean isImage = categories.get(position).getSrcImage() == null;

        // imageProduct. setImageURI(Uri.parse(categories[position].getSrcImage()) );
        if(isImage){
            Picasso.get().load(R.drawable.image_no_image).into(imageProduct);
        } else {
            Picasso.get().load(categories.get(position).getSrcImage()).into(imageProduct);
        }

        productTitleText.setText(categories.get(position).getTitle());

        productPriceText.setText(priceToString(categories.get(position).getPrice()));

        poductGradeText.setText(appRatingToString(categories.get(position).getAppRating()));
        return grid;
    }

    // возвращает содержимое выделенного элемента списка
//    public String getItem(int position) {
//        return contacts[position];
//    }

    //преобразует цену в строкое представление
    String priceToString(int price){
        int ruble = price / 100;
        int kopeck = price % 100;
        if (kopeck <= 9){
            return String.valueOf(ruble) + ".0" + String.valueOf(kopeck) + " Руб.";
        }
//        else if (kopeck % 10 == 0 || kopeck % 100 == 0){
//            return String.valueOf(ruble) + "." + String.valueOf(kopeck) + "0";
//        }

        return String.valueOf(ruble) + "." + String.valueOf(kopeck) + " Руб.";
    }

    //рейтинг превратить в строчное представленеи
    String appRatingToString(int rating){
        return String.valueOf(rating / 10) + "." + String.valueOf(rating % 10)
;    }
}
