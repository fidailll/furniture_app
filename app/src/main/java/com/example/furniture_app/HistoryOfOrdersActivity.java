package com.example.furniture_app;

import static android.view.View.GONE;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.example.furniture_app.adapter.HistoryOfOrdersAdapter;
import com.example.furniture_app.model.OrderModel;
import com.example.furniture_app.model.ProductModel;
import com.example.furniture_app.model.UserProductModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class HistoryOfOrdersActivity extends AppCompatActivity {

    private static final String TAG = "HistoryOfOrdersActivity";
    List<UserProductModel> cartList;

    RecyclerView recycler;
    FirebaseFirestore db;

    HistoryOfOrdersAdapter adapter;

    Button btMenu;
    Button buttonHistoryOfOrderBack;

    String id;
    String idKey;
    String idKeyOrder;
    FloatingActionButton btPay;

    OrderModel orderModel;

    static final String ACCESS_MESSAGE="ACCESS_MESSAGE";
    ActivityResultLauncher<Intent> mStartForResult = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if(result.getResultCode() == Activity.RESULT_OK){
                        Intent intent = result.getData();
                       String mess = intent.getStringExtra(ACCESS_MESSAGE);
                        orderModel.setPaid(Boolean.valueOf(mess));
                        Log.w(TAG, String.valueOf(mess));

                        if(orderModel.isPaid()) {
                            btPay.setVisibility( GONE);
                        }
                    }
                    else{
//                        textView.setText("Ошибка доступа");
                    }
                }
            });
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getIntent().getExtras();

        assert arguments != null;

        id = arguments.get("id").toString();

        idKey = arguments.get("idKey").toString();

        orderModel = (OrderModel) arguments.getSerializable(OrderModel.class.getName());

        setContentView(R.layout.activity_history_of_orders);
//        Objects.requireNonNull(getSupportActionBar()).hide();

        db = FirebaseFirestore.getInstance();

        btMenu = (Button) findViewById(R.id.buttonHistoryOfOrderFavorite);

        btPay = (FloatingActionButton ) findViewById(R.id.FLPay);



        if(orderModel.isPaid()) {
            btPay.setVisibility( GONE);
        }

        buttonHistoryOfOrderBack = (Button) findViewById(R.id.buttonHistoryOfOrderBack);


        onClickBack();

        onClickMenu();

        cartRecycler();

        onClickPayment();
    }

    private void onClickPayment(){
        btPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(HistoryOfOrdersActivity.this, PaymentActivity.class);

                intent.putExtra("idKey", idKey);
                intent.putExtra("idKeyOrder", idKeyOrder);
                intent.putExtra("sum", orderModel.getSum());

                mStartForResult.launch(intent);

            }
        });
    }

    private void onClickBack() {
        buttonHistoryOfOrderBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent data = new Intent();

                setResult(RESULT_OK, data);
                finish();
            }
        });
    }

    private void onClickMenu() {
        btMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(HistoryOfOrdersActivity.this, btMenu);
                popup.getMenuInflater().inflate(R.menu.sample_menu, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {

                        CancelOrderDialogFragment dialog = new CancelOrderDialogFragment(new CancelOrder() {
                            @Override
                            public void cancel() {
                                db.collection("users")
                                        .document(idKey)
                                        .collection("orders")
                                        .document(idKeyOrder).update("status", "Отменен").addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                Toast.makeText(HistoryOfOrdersActivity.this, "Заказ отменен!", Toast.LENGTH_SHORT).show();
                                            }
                                        });

                            }
                        });
                        dialog.show(HistoryOfOrdersActivity.this.getSupportFragmentManager(), "custom");
                        return true;
                    }
                });

                popup.show();
            }
        });
    }

    //Показывает товары
    private void cartRecycler() {
        cartList = new ArrayList<>();
        db.collection("users")
                .document(idKey)
                .collection("orders")
                .whereEqualTo("id", Integer.parseInt(id))
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@androidx.annotation.NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {

                                Log.d(TAG, document.getId() + " => " + document.getData());
                                idKeyOrder = document.getId();

//                                db.collection("users")
//                                        .document(idKey)
//                                        .collection("orders")
//                                        .document(idKeyOrder).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
//                                            @Override
//                                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
//                                                if (task.isSuccessful()) {
//
//                                                    Log.d(TAG, task.getResult().getData().toString());
//                                                }
//                                            }
//                                        });

                                db.collection("users")
                                        .document(idKey)
                                        .collection("orders")
                                        .document(idKeyOrder)
                                        .collection("order")
                                        .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                            @Override
                                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                if (task.isSuccessful()) {
                                                    for (QueryDocumentSnapshot document : task.getResult()) {

                                                        Log.d(TAG, document.getId() + " => " + document.getData());
                                                        Map<String, Object> objectMap = document.getData();

                                                        cartList.add(new UserProductModel(
                                                                Integer.parseInt(objectMap.get("id").toString()),
                                                                objectMap.get("title").toString(),
                                                                objectMap.get("description").toString(),
                                                                Integer.parseInt(objectMap.get("price").toString()),
                                                                objectMap.get("srcImage").toString(),
                                                                Integer.parseInt(objectMap.get("appRating").toString()),
                                                                objectMap.get("categoryName").toString(),
                                                                Integer.parseInt(objectMap.get("categoryId").toString()),
                                                                objectMap.get("manufacturer").toString(),
                                                                objectMap.get("guarantee").toString(),
                                                                objectMap.get("lifeTime").toString(),
                                                                objectMap.get("color").toString(),
                                                                Integer.parseInt(objectMap.get("length").toString()),
                                                                Integer.parseInt(objectMap.get("height").toString()),
                                                                Integer.parseInt(objectMap.get("width").toString()),
                                                                Integer.parseInt(objectMap.get("count").toString()))
                                                        );
                                                    }
                                                    HistoryOfOrdersRecycler(cartList);
                                                } else {
                                                    Log.w(TAG, "Error getting documents.", task.getException());
                                                    Toast.makeText(getApplicationContext(),
                                                            "Ошибка!", Toast.LENGTH_SHORT).show();
                                                }

                                            }
                                        });


                            }


                        } else {
                            Log.w(TAG, "Error getting documents.", task.getException());
                            Toast.makeText(getApplicationContext(),
                                    "Ошибка!", Toast.LENGTH_SHORT).show();
                        }
                    }

                });


    }

    private void HistoryOfOrdersRecycler(List<UserProductModel> products) {

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);

        recycler = (RecyclerView) findViewById(R.id.recyclerViewHistoryOfOrder);

        recycler.setLayoutManager(layoutManager);

        adapter = new HistoryOfOrdersAdapter(this, products);

        recycler.setAdapter(adapter);

    }
}