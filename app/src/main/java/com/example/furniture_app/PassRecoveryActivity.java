package com.example.furniture_app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Objects;

public class PassRecoveryActivity extends AppCompatActivity {

    private static final String TAG = "PassRecoveryActivity";
    Button buttonProductBack;
    Button buttonSend;
    TextInputLayout textInputEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pass_recovery);
//        Objects.requireNonNull(getSupportActionBar()).hide();

        buttonProductBack = findViewById(R.id.buttonPassRecoveryBack);
        buttonSend = findViewById(R.id.buttonSendPassRecovery);
        textInputEmail = findViewById(R.id.textInputEmailPassRecovery);

        onClickBack();
        onClickSend();
    }

    //кнопка назад
    private void onClickBack() {
        buttonProductBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void onClickSend() {
        buttonSend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String email = textInputEmail.getEditText().getText().toString();
                        checkEditText(email);
                    }
                }
        );
    }

    /// Проверка на наличия текста в EditText
    private void checkEditText(String email) {
        if (email.length() == 0) {
            Toast.makeText(PassRecoveryActivity.this, "Заполните поле Email!",
                    Toast.LENGTH_SHORT).show();
            textInputEmail.getEditText().setError("Заполните поле Email!");
        } else {
            sendAPasswordResetEmail(email);
        }
    }

    /// Отправить электронное письмо для сброса пароля
    private void sendAPasswordResetEmail(String email){
        FirebaseAuth auth = FirebaseAuth.getInstance();
        //String emailAddress = "user@example.com";

        auth.sendPasswordResetEmail(email)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(PassRecoveryActivity.this, "Письмо отправлено на ваш электронный ящик!",
                                    Toast.LENGTH_SHORT).show();
                            Log.d(TAG, "Email sent.");
                            textInputEmail.getEditText().setText("");
                        } else {
                            Toast.makeText(PassRecoveryActivity.this, "Не получилось отправить письмо!",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}