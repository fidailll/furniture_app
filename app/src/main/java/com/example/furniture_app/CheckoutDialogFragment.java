package com.example.furniture_app;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

public class CheckoutDialogFragment extends DialogFragment {

    private Order order;

    public CheckoutDialogFragment(Order order) {
        this.order = order;
    }

    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());

        return builder.setTitle("Вы действительно хотите оформить заказ?").setMessage("Общая сумма: "+ priceToString(order.getSum()) + " Заказ будет ожидать по адресу Россия г. Стерлитамак, проспект Ленина, 37").setPositiveButton("Оформить", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        order.setOrder();
                    }
                })
                .setNegativeButton("Отмена", null).create();
    }

    //преобразует цену в строкое представление
    String priceToString(int price){
        int ruble = price / 100;
        int kopeck = price % 100;
        if (kopeck <= 9){
            return String.valueOf(ruble) + ".0" + String.valueOf(kopeck) + " Руб.";
        }
//        else if (kopeck % 10 == 0 || kopeck % 100 == 0){
//            return String.valueOf(ruble) + "." + String.valueOf(kopeck) + "0";
//        }

        return String.valueOf(ruble) + "." + String.valueOf(kopeck) + " Руб.";
    }
}
