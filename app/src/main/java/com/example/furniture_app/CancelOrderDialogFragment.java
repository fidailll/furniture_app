package com.example.furniture_app;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

public class CancelOrderDialogFragment extends DialogFragment {

    public CancelOrderDialogFragment(CancelOrder cancelOrder) {
        this.cancelOrder = cancelOrder;
    }

    CancelOrder cancelOrder;


    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());

        return builder.setTitle("Отменить заказ?")
                .setPositiveButton("Отменить", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        cancelOrder.cancel();
                    }
                })
                .setNegativeButton("Отмена", null).create();
    }
}
