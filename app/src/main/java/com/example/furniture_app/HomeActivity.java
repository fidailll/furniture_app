package com.example.furniture_app;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.furniture_app.databinding.ActivityHomeBinding;
import com.example.furniture_app.model.ProductModel;
import com.example.furniture_app.ui.cart.CartFragment;
import com.example.furniture_app.ui.home.HomeFragment;
import com.example.furniture_app.ui.profile.ProfileFragment;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.MenuItemKt;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.Objects;

public class HomeActivity extends AppCompatActivity implements LogOut {

    private static final String TAG = "HomeActivity";
    private ActivityHomeBinding binding;

    String idKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home);

        loadNavFragment(new HomeFragment());
       BottomNavigationView navView = findViewById(R.id.nav_view);

        navView.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                int id = item.getItemId() ;
                if (id == R.id.navigation_home) {
                    loadNavFragment(new HomeFragment());
                    return true;
                } else if (id == R.id.navigation_cart) {
                    loadNavFragment(new CartFragment());
                    return true;
                } else if(id ==R.id.navigation_profile)
                { loadNavFragment(new ProfileFragment());
                    return true;
                }
                else {
                    return false;
                }

            }
        });
    }
    private void  loadNavFragment(Fragment fragment){
        //This funcation for load fragment directly

        FragmentManager fragmentManager= getSupportFragmentManager();
        FragmentTransaction fragmentTransaction= fragmentManager.beginTransaction();

        //use replace
        fragmentTransaction.replace(R.id.nav_host_fragment_activity_home, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void call() {
        FirebaseAuth.getInstance().signOut();
        Intent intent = new Intent(HomeActivity.this, WelcomeActivity.class);
        startActivity(intent);
        finish();
    }



}