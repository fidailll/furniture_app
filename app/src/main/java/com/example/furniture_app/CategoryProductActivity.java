package com.example.furniture_app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.example.furniture_app.adapter.ProductAdapterGrid;
import com.example.furniture_app.model.CategoryModel;
import com.example.furniture_app.model.ProductModel;
import com.example.furniture_app.ui.home.HomeFragment;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class CategoryProductActivity extends AppCompatActivity {
    GridView productsGrid;
    FirebaseFirestore db;
    private static final String TAG = "CategoryProductActivity";
    List<ProductModel> productModel = new ArrayList<>();
//    = [
//            new ProductModel(1, "Test 1", "Тестовое описание", 10000, "https://toriani.ru/upload/iblock/921/9210cc48edc688f7d9cff7fdfbb4c2d4.jpg", 50, "Стул", 1),
//            new ProductModel(2, "Test 2", "Тестовое описание", 10000, "https://chiedocover.ru/statics/product/4860/5df0829fc625c.jpg", 50, "Стул", 1),
//            new ProductModel(3, "Test 3", "Тестовое описание", 10000, "https://materialyinfo.ru/wp-content/uploads/2017/12/Derevyannyie-stulya-84.jpg", 50, "Стул", 1),
//            new ProductModel(4, "Test 4", "Тестовое описание", 10000, "https://osnovabanketa.ru/statics/product/1657/61cd751bc01b6.jpg", 50, "Стул", 1),
//            new ProductModel(5, "Test 5", "Тестовое описание", 10000, "https://toriani.ru/upload/iblock/629/6291f0b49bb5d99b08ba0647f697f389.jpg", 50, "Стул", 1),
//            new ProductModel(6, "Test 6", "Тестовое описание", 10000, "https://ctt-online.ru/image/cache/catalog/products/6/rket-ru-upload-shop_1-2-6-6-item_266566-shop_items_catalog_image266566-570x741.jpg", 50, "Стул", 1),
//            new ProductModel(7, "Test 6", "Тестовое описание", 10000, "https://ctt-online.ru/image/cache/catalog/products/6/rket-ru-upload-shop_1-2-6-6-item_266566-shop_items_catalog_image266566-570x741.jpg", 50, "Стул", 1),
//            new ProductModel(8, "Test 6", "Тестовое описание", 10000, "https://ctt-online.ru/image/cache/catalog/products/6/rket-ru-upload-shop_1-2-6-6-item_266566-shop_items_catalog_image266566-570x741.jpg", 50, "Стул", 1),
//];
    CategoryModel categoryModel;

    Button buttonCategoryProductActivityBack;

    TextView title;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle arguments = getIntent().getExtras();

        assert arguments != null;

        categoryModel = (CategoryModel) arguments.getSerializable(CategoryModel.class.getName());


        setContentView(R.layout.activity_category_product);

        db = FirebaseFirestore.getInstance();

//        Objects.requireNonNull(getSupportActionBar()).hide();

        title = (TextView) findViewById(R.id.textCategoryProductTitle) ;

        buttonCategoryProductActivityBack = (Button) findViewById(R.id.buttonCategoryProductBack);

        title.setText(categoryModel.getName());
        productAdapterGrid();
        onBack();
    }

    private void onBack(){
        buttonCategoryProductActivityBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    //Выгружает отсортированный список с нужной категорией
    private void productAdapterGrid(){
        productsGrid =  findViewById(R.id.gridCategoryProduct);

        //productModel.add( new ProductModel(1, "Test 1", "Тестовое описание", 10000, "https://toriani.ru/upload/iblock/921/9210cc48edc688f7d9cff7fdfbb4c2d4.jpg", 50, "Стул", 1));


        db.collection("products").whereEqualTo("categoryId", categoryModel.getId()).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {

                    for (QueryDocumentSnapshot document : task.getResult()) {

                        Log.d(TAG, document.getId() + " => " + document.getData());
                        Map<String, Object> objectMap = document.getData();
//                        Log.d(TAG, objectMap.get("appRating").toString());

                        productModel.add(new ProductModel(
                                Integer.parseInt(objectMap.get("id").toString()),
                                objectMap.get("title").toString(),
                                objectMap.get("description").toString(),
                                Integer.parseInt(objectMap.get("price").toString()),
                                objectMap.get("srcImage").toString(),
                                Integer.parseInt(objectMap.get("appRating").toString()),
                                objectMap.get("categoryName").toString(),
                                Integer.parseInt(objectMap.get("categoryId").toString()),
                                objectMap.get("manufacturer").toString(),
                                objectMap.get("guarantee").toString(),
                                objectMap.get("lifeTime").toString(),
                                objectMap.get("color").toString(),
                                Integer.parseInt(objectMap.get("length").toString()),
                                Integer.parseInt(objectMap.get("height").toString()),
                                Integer.parseInt(objectMap.get("width").toString())
                                ));

                    }

                    ProductAdapterGrid mAdapter = new ProductAdapterGrid(CategoryProductActivity.this,
                            android.R.layout.simple_list_item_1, productModel);



                    productsGrid.setAdapter(mAdapter);


                    productsGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View v,
                                                int position, long id) {

                            Intent intent = new Intent(CategoryProductActivity.this, ProductActivity.class);
                            // передаем индекс массива
                            intent. putExtra(ProductModel.class.getName(), mAdapter.getItem(position));

                            startActivity(intent);
                        }
                    });

                } else {
                    Log.w(TAG, "Error getting documents.", task.getException());
                }
            }
        });


    }
}

