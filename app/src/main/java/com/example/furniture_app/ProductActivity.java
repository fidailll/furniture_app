package com.example.furniture_app;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.furniture_app.model.ProductModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ProductActivity extends AppCompatActivity {

    private static final String TAG = "ProductActivity";
    Button buttonProductBack;
    Button buttonProductFavorite;
    ImageView imageProduct;
    TextView textProductName;
    TextView textProductGrade;
    TextView textProductSale;
    TextView textProductCategory;
    TextView textProductDescription;
    TextView textManufacturer;
    TextView textGuarantee;
    TextView textLifeTime;
    TextView textColor;
    TextView textLength;
    TextView textHeight;
    TextView textWidth;


    FirebaseFirestore db;
    ProductModel product;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Bundle arguments = getIntent().getExtras();

        assert arguments != null;

        product = (ProductModel) arguments.getSerializable(ProductModel.class.getName());



        setContentView(R.layout.activity_product);
//        Objects.requireNonNull(getSupportActionBar()).hide();
        db = FirebaseFirestore.getInstance();


        buttonProductBack = (Button) findViewById(R.id.buttonProductBack);
        buttonProductFavorite = (Button) findViewById(R.id.buttonProductFavorite);

        imageProduct = (ImageView) findViewById(R.id.imageProduct);

        textProductName = (TextView) findViewById(R.id.textProductName);
        textProductGrade = (TextView) findViewById(R.id.textProductGrade);
        textProductSale = (TextView) findViewById(R.id.textProductSale);
        textProductCategory = (TextView) findViewById(R.id.textProductCategory);
        textProductDescription = (TextView) findViewById(R.id.textProductDescription);

        textManufacturer = (TextView) findViewById(R.id.textManufacturer);
        textGuarantee = (TextView) findViewById(R.id.textGuarantee);
        textLifeTime = (TextView) findViewById(R.id.textLifeTime);
        textColor = (TextView) findViewById(R.id.textColor);
        textLength = (TextView) findViewById(R.id.textLength);
        textHeight = (TextView) findViewById(R.id.textHeight);
        textWidth = (TextView) findViewById(R.id.textWidth);

        boolean isImage = product.getSrcImage() == null;

        if (isImage) {
            Picasso.get().load(R.drawable.image_no_image).into(imageProduct);
        } else {
            Picasso.get().load(product.getSrcImage()).into(imageProduct);
        }

        textProductName.setText(product.getTitle());
        textProductGrade.setText(appRatingToString(product.getAppRating()));
        textProductSale.setText(priceToString(product.getPrice()));
        textProductCategory.setText(product.getCategoryName());
        textProductDescription.setText(product.getDescription());
        textManufacturer.setText(product.getManufacturer());
        textGuarantee.setText(product.getGuarantee());
        textLifeTime.setText(product.getLifeTime());
        textColor.setText(product.getColor());
        textLength.setText(String.valueOf(product.getLength()));
        textHeight.setText(String.valueOf(product.getHeight()));
        textWidth.setText(String.valueOf(product.getWidth()));

        onClickBack();

    }

    private void onClickBack() {
        buttonProductBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    //Добавление в корзину
    public void onClickProduct(View v) {
        db.collection("users")
                .whereEqualTo("id", FirebaseAuth.getInstance().getCurrentUser().getUid())
                .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@androidx.annotation.NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {

                                Log.d(TAG, document.getId() + " => " + document.getData());
                                String idKey = document.getId();

//                                boolean isEmpty = db.collection("users").document(idKey).collection("products").whereEqualTo("id", product.getId()).get().getResult().isEmpty();

                                //проверка на наличие товара в корзине
//                                if (isEmpty) {
                                addShop(idKey);
//                                } else {
//                                    Toast.makeText(getApplicationContext(),
//                                            "Уже добавлено в корзине!", Toast.LENGTH_SHORT).show();
//                                }

                            }
                        } else {
                            Log.w(TAG, "Error getting documents.", task.getException());
                            Toast.makeText(getApplicationContext(),
                                    "Ошибка!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });


    }

    //добавляет товар в корзину пользователя
    void addShop(String idKey) {

        Map<String, Object> data = new HashMap<>();

        data.put("count", 1);
        data.put("id", product.getId());
        data.put("title", product.getTitle());
        data.put("description", product.getDescription());
        data.put("price", product.getPrice());
        data.put("srcImage", product.getSrcImage());
        data.put("appRating", product.getAppRating());
        data.put("categoryName", product.getCategoryName());
        data.put("categoryId", product.getCategoryId());
        data.put("manufacturer", product.getManufacturer());
        data.put("guarantee", product.getGuarantee());
        data.put("lifeTime", product.getLifeTime());
        data.put("color", product.getColor());
        data.put("length", product.getLength());
        data.put("height", product.getHeight());
        data.put("width", product.getWidth());



        db.collection("users")
                .document(idKey)
                .collection("products")
                .document(String.valueOf(product.getId()))
                .set(data)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "DocumentSnapshot successfully written!");
                        Toast.makeText(getApplicationContext(),
                                "Добавлено в корзину!", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure( Exception e) {
                        Toast.makeText(getApplicationContext(),
                                "Не удалось добавить в корзину!", Toast.LENGTH_SHORT).show();
                        Log.w(TAG, "Error writing document", e);
                    }
                });

    }

    //преобразует цену в строкое представление
    String priceToString(int price) {
        int ruble = price / 100;
        int kopeck = price % 100;
        Log.d("ProductActivity", String.valueOf(kopeck));
        if (kopeck <= 9) {
            return String.valueOf(ruble) + ".0" + String.valueOf(kopeck) + " Руб.";
        }
//        else if (kopeck % 10 == 0){
//            return String.valueOf(ruble) + "." + String.valueOf(kopeck) + "0";
//        }

        return String.valueOf(ruble) + "." + String.valueOf(kopeck) + " Руб.";
    }

    //рейтинг превратить в строчное представленеи
    String appRatingToString(int rating) {
        return String.valueOf(rating / 10) + "." + String.valueOf(rating % 10)
                ;
    }

}