package com.example.furniture_app;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.annotation.NonNull;

public class ExitToAppDialogFragment extends DialogFragment {
   private LogOut logOut;
    @Override
    public void onAttach(@NonNull Context context){
        super.onAttach(context);
        logOut = (LogOut) getActivity();
    }
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());

        return builder.setTitle("Вы действительно хотите выйти из аккаунта?").setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                logOut.call();
            }
        })
                .setNegativeButton("Отмена", null).create();
    }



}
