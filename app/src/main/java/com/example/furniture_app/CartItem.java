package com.example.furniture_app;

import com.example.furniture_app.model.ProductModel;
import com.example.furniture_app.model.UserProductModel;

//Карточка с товаром
public interface CartItem {
    void delete(UserProductModel productModel);

    void add(UserProductModel productModel);

    void minimize(UserProductModel productModel);
}
