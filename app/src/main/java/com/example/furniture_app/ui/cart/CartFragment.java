package com.example.furniture_app.ui.cart;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.furniture_app.CartItem;
import com.example.furniture_app.CheckoutDialogFragment;
import com.example.furniture_app.Order;
import com.example.furniture_app.adapter.CartAdapter;
import com.example.furniture_app.databinding.FragmentCartBinding;
import com.example.furniture_app.model.UserProductModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class CartFragment extends Fragment {

    private static final String TAG = "CartFragment";
    private FragmentCartBinding binding;

    private List<UserProductModel> cartList = new ArrayList<>();

    RecyclerView cartRecycler;

    CartAdapter cartAdapter;

    FloatingActionButton floatingActionButtonCart;

    FirebaseFirestore db;

    String idKey;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
//        CartViewModel dashboardViewModel =
//                new ViewModelProvider(this).get(CartViewModel.class);

        binding = FragmentCartBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        db = FirebaseFirestore.getInstance();

        floatingActionButtonCart = binding.floatingActionButtonCart;

        onClickFloatingActionButtonCart();

        db.collection("users")
                .whereEqualTo("id", FirebaseAuth.getInstance().getCurrentUser().getUid())
                .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@androidx.annotation.NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {

                                Log.d(TAG, document.getId() + " => " + document.getData());
                                idKey = document.getId();

                                cartRecycler(idKey);

                            }
                        } else {
                            Log.w(TAG, "Error getting documents.", task.getException());
                            Toast.makeText(getContext(),
                                    "Ошибка!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

        return root;
    }

    //Показывает товары из корзины
    private void cartRecycler(String idKey) {

        db.collection("users")
                .document(idKey)
                .collection("products")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@androidx.annotation.NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {

                                Log.d(TAG, document.getId() + " => " + document.getData());
                                Map<String, Object> objectMap = document.getData();

                                cartList.add(new UserProductModel(
                                        Integer.parseInt(objectMap.get("id").toString()),
                                        objectMap.get("title").toString(),
                                        objectMap.get("description").toString(),
                                        Integer.parseInt(objectMap.get("price").toString()),
                                        objectMap.get("srcImage").toString(),
                                        Integer.parseInt(objectMap.get("appRating").toString()),
                                        objectMap.get("categoryName").toString(),
                                        Integer.parseInt(objectMap.get("categoryId").toString()),
                                        objectMap.get("manufacturer").toString(),
                                        objectMap.get("guarantee").toString(),
                                        objectMap.get("lifeTime").toString(),
                                        objectMap.get("color").toString(),
                                        Integer.parseInt(objectMap.get("length").toString()),
                                        Integer.parseInt(objectMap.get("height").toString()),
                                        Integer.parseInt(objectMap.get("width").toString()),
                                        Integer.parseInt(objectMap.get("count").toString()))
                                );
                            }

                            setCartRecycler(cartList);

                        } else {
                            Log.w(TAG, "Error getting documents.", task.getException());
                            Toast.makeText(getContext(),
                                    "Ошибка!", Toast.LENGTH_SHORT).show();
                        }
                    }

                });


//        setCartRecycler(cartList);

    }

    private void onClickFloatingActionButtonCart() {
        floatingActionButtonCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cartList.isEmpty()) {
                    Toast.makeText(binding.getRoot().getContext(),
                            "Пустая корзина!", Toast.LENGTH_SHORT).show();
                } else {
                CheckoutDialogFragment dialog = new CheckoutDialogFragment(new Order() {

                    @Override
                    public void setOrder() {
                        createAnOrder();
                    }

                    @Override
                    public int getSum() {
                        int sum = 0;
                        for (int i = 0; i < cartList.size(); i++) {
                            sum = sum + (cartList.get(i).getPrice() * cartList.get(i).getCount());
                        }
                        return sum;
                    }
                });
                dialog.show(getActivity().getSupportFragmentManager(), "custom");


                }
//                Toast.makeText(binding.getRoot().getContext(),
//                        "Заказ обрабатывается", Toast.LENGTH_SHORT).show();
//                cartList.clear();
//
//                setCartRecycler(cartList);
            }
        });
    }


    private void setCartRecycler(List<UserProductModel> products) {

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);

        cartRecycler = binding.cartRecyclerView;
        cartRecycler.setLayoutManager(layoutManager);

        cartAdapter = new CartAdapter(getContext(), products, new CartItem() {
            @Override
            public void delete(UserProductModel productModel) {
                db.collection("users")
                        .document(idKey)
                        .collection("products").document(String.valueOf(productModel.getId())).delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                Toast.makeText(binding.getRoot().getContext(),
                                        "Удалено!", Toast.LENGTH_SHORT).show();
                            }
                        });
            }

            @Override
            public void add(UserProductModel productModel) {
                db.collection("users")
                        .document(idKey)
                        .collection("products").document(String.valueOf(productModel.getId())).update("count", productModel.getCount());
            }

            @Override
            public void minimize(UserProductModel productModel) {

                if (productModel.getCount() > 1) {
                    db.collection("users")
                            .document(idKey)
                            .collection("products").document(String.valueOf(productModel.getId())).update("count", (productModel.getCount()));
                }

            }
        });

        cartRecycler.setAdapter(cartAdapter);

        int itemCount = cartAdapter.getItemCount();
        if(itemCount > 0)
            binding.emptyText.setVisibility(View.GONE);
        else
            binding.emptyText.setVisibility(View.VISIBLE);

    }

    //Метод загружающий заказ
    private void createAnOrder() {

            int id = (int) System.currentTimeMillis();

            Date currentDate = new Date();
            DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
            String dateText = dateFormat.format(currentDate);

            Map<String, Object> data = new HashMap<>();

            int sum = 0;

            for (int i = 0; i < cartList.size(); i++) {
                sum = sum + (cartList.get(i).getPrice() * cartList.get(i).getCount());
            }

            data.put("id", id);
            data.put("date", dateText);
            data.put("address", "Россия г. Стерлитамак, проспект Ленина, 37");
            data.put("sum", sum);
            data.put("status", "Ждет оплаты!");
            data.put("date", dateText);
            data.put("isPaid", false);
            db.collection("users")
                    .document(idKey)
                    .collection("orders").add(data)
                    .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                        @Override
                        public void onSuccess(DocumentReference documentReference) {
                            Log.d(TAG, "DocumentSnapshot written with ID: " + documentReference.getId());
                            downloadOrderDetails(documentReference.getId());
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.w(TAG, "Error adding document", e);
                        }
                    });


    }

    //Загрузка из корзины товаров в заказ
    private void downloadOrderDetails(String key) {
        for (int i = 0; i < cartList.size(); i++) {

            Map<String, Object> data = new HashMap<>();

            data.put("count", cartList.get(i).getCount());
            data.put("id", cartList.get(i).getId());
            data.put("title", cartList.get(i).getTitle());
            data.put("description", cartList.get(i).getDescription());
            data.put("price", cartList.get(i).getPrice());
            data.put("srcImage", cartList.get(i).getSrcImage());
            data.put("appRating", cartList.get(i).getAppRating());
            data.put("categoryName", cartList.get(i).getCategoryName());
            data.put("categoryId", cartList.get(i).getCategoryId());
            data.put("manufacturer", cartList.get(i).getManufacturer());
            data.put("guarantee", cartList.get(i).getGuarantee());
            data.put("lifeTime", cartList.get(i).getLifeTime());
            data.put("color", cartList.get(i).getColor());
            data.put("length", cartList.get(i).getLength());
            data.put("height", cartList.get(i).getHeight());
            data.put("width", cartList.get(i).getWidth());

            db.collection("users")
                    .document(idKey)
                    .collection("orders").document(key).collection("order").add(data);
            db.collection("users")
                    .document(idKey).collection("products").document(String.valueOf(cartList.get(i).getId())).delete();

        }
        Toast.makeText(binding.getRoot().getContext(),
                "Заказ успешно оформлен!", Toast.LENGTH_SHORT).show();
        cartList.clear();

        setCartRecycler(cartList);


//        db.collection("users")
//                .document(idKey).collection("products").document().delete().addOnCompleteListener(new OnCompleteListener<Void>() {
//                    @Override
//                    public void onComplete(@NonNull Task<Void> task) {
//

//                    }
//                });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }


}