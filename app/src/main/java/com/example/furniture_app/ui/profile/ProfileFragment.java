package com.example.furniture_app.ui.profile;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.furniture_app.HistoryOfOrdersActivity;
import com.example.furniture_app.OrderActivity;
import com.example.furniture_app.R;
import com.example.furniture_app.databinding.FragmentProfileBinding;
import com.example.furniture_app.ExitToAppDialogFragment;
import com.example.furniture_app.model.CategoryModel;
import com.example.furniture_app.model.UserModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.search.SearchBar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.picasso.Picasso;

import java.util.Map;

public class ProfileFragment extends Fragment {

    private FragmentProfileBinding binding;
    private static final String TAG = "ProfileFragment";
    Button historyShopButton;
    Button changePasswordButton;
    Button exitToAppButton;
    TextView textName;
    TextView textEmail;
    ImageView imageAvatar;

    String idKey;

    FirebaseFirestore db;
    DatabaseReference usersDatabaseReference;
//    ChildEventListener usersChildEventListener;



    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentProfileBinding.inflate(inflater, container, false);

        View root = binding.getRoot();

        historyShopButton = binding.historyShopButton;
        changePasswordButton = binding.changePasswordButton;
        exitToAppButton = binding.exitToAppButton;

        textName = binding.nameProfile;
        textEmail = binding.emailProfile;

        imageAvatar = binding.imageProfile;


        onClickHistory();
        onClickExitToApp();

        db = FirebaseFirestore.getInstance();


        getProfile();


        return root;


    }


    /// Выгрузка профиля из Firestore
    private void getProfile(){
        db.collection("users").whereEqualTo("id", FirebaseAuth.getInstance().getCurrentUser().getUid()).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {

                        Log.d(TAG, document.getId() + " => " + document.getData());

                        idKey = document.getId();

                        Map<String, Object> objectMap = document.getData();
                        textName.setText(objectMap.get("name").toString());
                        textEmail.setText(objectMap.get("email").toString());

                        boolean isImage = objectMap.get("photo") == "";


                        if(isImage){
                            Picasso.get().load(R.drawable.image_no_image).into(imageAvatar);
                        } else {
                            Picasso.get().load(objectMap.get("photo").toString()).into(imageAvatar);
                        }

                    }
                } else {
                    Log.w(TAG, "Error getting documents.", task.getException());
                }
            }
        });
    }

    private void onClickExitToApp(){
        exitToAppButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExitToAppDialogFragment dialog = new ExitToAppDialogFragment();
                dialog.show(getActivity().getSupportFragmentManager(), "custom");
            }
        });
    }

    private void onClickHistory(){
        historyShopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileFragment.this.getActivity(), OrderActivity.class);
                intent.putExtra("idKey", idKey);

                startActivity(intent);

            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
    //выход из аккаунта

}