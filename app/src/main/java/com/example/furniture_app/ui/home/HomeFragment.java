package com.example.furniture_app.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.furniture_app.HistoryOfOrdersActivity;
import com.example.furniture_app.ProductActivity;
import com.example.furniture_app.R;
import com.example.furniture_app.SearchActivity;
import com.example.furniture_app.adapter.CategoryAdapter;
import com.example.furniture_app.adapter.ProductAdapterGrid;
import com.example.furniture_app.databinding.FragmentHomeBinding;
import com.example.furniture_app.model.CategoryModel;
import com.example.furniture_app.model.ProductModel;
import com.example.furniture_app.ui.profile.ProfileFragment;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.search.SearchBar;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class HomeFragment extends Fragment implements
        AdapterView.OnItemSelectedListener {

    private static final String TAG = "HomeFragment";
    private FragmentHomeBinding binding;

    RecyclerView categoryRecycler;

    GridView productsGrid;
    CategoryAdapter categoryAdapter;


    SearchBar searchBar;

    FirebaseFirestore db;

    private List<ProductModel> categories = new ArrayList<>();
//            = {
//            new ProductModel(1, "Test 1", "Тестовое описание", 10000, "https://toriani.ru/upload/iblock/921/9210cc48edc688f7d9cff7fdfbb4c2d4.jpg", 50, "Стул", 1),
//            new ProductModel(2, "Test 2", "Тестовое описание", 10000, "https://chiedocover.ru/statics/product/4860/5df0829fc625c.jpg", 50, "Стул", 1),
//            new ProductModel(3, "Test 3", "Тестовое описание", 10000, "https://materialyinfo.ru/wp-content/uploads/2017/12/Derevyannyie-stulya-84.jpg", 50, "Стул", 1),
//            new ProductModel(4, "Test 4", "Тестовое описание", 10000, "https://osnovabanketa.ru/statics/product/1657/61cd751bc01b6.jpg", 50, "Стул", 1),
//            new ProductModel(5, "Test 5", "Тестовое описание", 10000, "https://toriani.ru/upload/iblock/629/6291f0b49bb5d99b08ba0647f697f389.jpg", 50, "Стул", 1),
//            new ProductModel(6, "Test 6", "Тестовое описание", 10000, "https://ctt-online.ru/image/cache/catalog/products/6/rket-ru-upload-shop_1-2-6-6-item_266566-shop_items_catalog_image266566-570x741.jpg", 50, "Стул", 1),
//
//    };


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {


        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        searchBar = binding.searchBar;

        db = FirebaseFirestore.getInstance();

        categoryRecycler();

        productAdapterGrid();

        onClickSearch();

        return root;

    }

    //Нажатие на поле поиска
    void onClickSearch(){
        searchBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeFragment.this.getActivity(), SearchActivity.class);
                startActivity(intent);

            }
        });
    }


    //Добавляет категории из firebase
    private void categoryRecycler() {

        List<CategoryModel> categoryList = new ArrayList<>();
        db.collection("categories").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {

                        Log.d(TAG, document.getId() + " => " + document.getData());
                        Map<String, Object> objectMap = document.getData();

                        categoryList.add(new CategoryModel(Integer.parseInt(objectMap.get("id").toString()), objectMap.get("name").toString(), objectMap.get("srcPhoto").toString()));
                    }
                    setCategoryRecycler(categoryList);
                } else {
                    Log.w(TAG, "Error getting documents.", task.getException());
                }
            }
        });

//        categoryList.add(new CategoryModel(1, "Диваны", R.drawable.baseline_chair_50));
//        categoryList.add(new CategoryModel(2, "Столы", R.drawable.baseline_desk_24));
//        categoryList.add(new CategoryModel(2, "Стулья", R.drawable.baseline_chair_alt_50));
//        categoryList.add(new CategoryModel(1, "Диваны", "https://firebasestorage.googleapis.com/v0/b/furniture-app-d4057.appspot.com/o/categories%2F%D0%9A%D0%BB%D1%83%D0%B1%D0%BD%D0%BE%D0%B5%20%D0%9A%D1%80%D0%B5%D1%81%D0%BB%D0%BE.png?alt=media&token=493739dc-373a-4ef0-a599-17a3e3843a73"));
//        categoryList.add(new CategoryModel(2, "Столы", "https://firebasestorage.googleapis.com/v0/b/furniture-app-d4057.appspot.com/o/categories%2F%D0%9A%D0%BE%D1%84%D0%B5%D0%B9%D0%BD%D1%8B%D0%B9%20%D0%A1%D1%82%D0%BE%D0%BB%D0%B8%D0%BA.png?alt=media&token=03e47788-53db-4b04-8904-539e5578f8a6"));
//        categoryList.add(new CategoryModel(3, "Стулья", "https://firebasestorage.googleapis.com/v0/b/furniture-app-d4057.appspot.com/o/categories%2F%D0%A1%D1%82%D1%83%D0%BB%202.png?alt=media&token=6472dc72-b020-4d29-aaea-138fe509f22e"));
//        categoryList.add(new Category(3, R.drawable.baseline_desk_24));
//        categoryList.add(new Category(4, R.drawable.baseline_desk_24));
//        categoryList.add(new Category(5, R.drawable.baseline_desk_24));
//        categoryList.add(new Category(6, R.drawable.baseline_desk_24));
//        categoryList.add(new Category(7, R.drawable.baseline_desk_24));
//        categoryList.add(new Category(8, R.drawable.baseline_desk_24));
//        categoryList.add(new Category(9, R.drawable.baseline_desk_24));
//        categoryList.add(new Category(10, R.drawable.baseline_desk_24));
//        categoryList.add(new Category(11, R.drawable.baseline_desk_24));
//        categoryList.add(new Category(12, R.drawable.baseline_desk_24));
    }

    ///Популярные товары из firebase
    private void productAdapterGrid() {
//       final ProductModel[] categories;

        productsGrid = binding.gridItems;

//        categories.add(new ProductModel(1, "Test 1", "Тестовое описание", 10000, "https://toriani.ru/upload/iblock/921/9210cc48edc688f7d9cff7fdfbb4c2d4.jpg", 50, "Стул", 1));
        db.collection("products").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {

                    for (QueryDocumentSnapshot document : task.getResult()) {

                        Log.d(TAG, document.getId() + " => " + document.getData());
                        Map<String, Object> objectMap = document.getData();
//                        Log.d(TAG, objectMap.get("appRating").toString());

                        categories.add(new ProductModel(
                                Integer.parseInt(objectMap.get("id").toString()),
                                objectMap.get("title").toString(),
                                objectMap.get("description").toString(),
                                Integer.parseInt(objectMap.get("price").toString()),
                                objectMap.get("srcImage").toString(),
                                Integer.parseInt(objectMap.get("appRating").toString()),
                                objectMap.get("categoryName").toString(),
                                Integer.parseInt(objectMap.get("categoryId").toString()),
                                objectMap.get("manufacturer").toString(),
                                objectMap.get("guarantee").toString(),
                                objectMap.get("lifeTime").toString(),
                                objectMap.get("color").toString(),
                                Integer.parseInt(objectMap.get("length").toString()),
                                Integer.parseInt(objectMap.get("height").toString()),
                                Integer.parseInt(objectMap.get("width").toString())));
                    }

                    ProductAdapterGrid mAdapter = new ProductAdapterGrid(getContext(),
                            android.R.layout.simple_list_item_1, categories);

                    productsGrid.setAdapter(mAdapter);


                    productsGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View v,
                                                int position, long id) {

                            Intent intent = new Intent(HomeFragment.this.getActivity(), ProductActivity.class);
                            // передаем индекс массива
                            intent.putExtra(ProductModel.class.getName(), mAdapter.getItem(position));

                            startActivity(intent);
                        }
                    });
                } else {
                    Log.w(TAG, "Error getting documents.", task.getException());
                }
            }
        });

    }


    private void setCategoryRecycler(List<CategoryModel> categoryList) {
        // определяем слушателя нажатия элемента в списке

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);

        categoryRecycler = binding.categoryRecycler;

        categoryRecycler.setLayoutManager(layoutManager);

        categoryAdapter = new CategoryAdapter(getContext(), categoryList);

        categoryRecycler.setAdapter(categoryAdapter);


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View v, int position,
                               long id) {
        // mSelectText.setText("Выбранный элемент: " + mAdapter.getItem(position));
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // mSelectText.setText("Выбранный элемент: ничего");
    }
}