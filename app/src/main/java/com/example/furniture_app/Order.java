package com.example.furniture_app;

public interface Order {
    void setOrder();

    int getSum();
}
