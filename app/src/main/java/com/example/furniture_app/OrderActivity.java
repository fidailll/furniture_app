package com.example.furniture_app;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.furniture_app.adapter.OrderAdapter;
import com.example.furniture_app.model.OrderModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class OrderActivity extends AppCompatActivity {

    FirebaseFirestore db;
    private static final String TAG = "OrderActivity";

    Button btOrdersBack;

    List<OrderModel> orders;

    RecyclerView recyclerOrders;

    OrderAdapter orderAdapter;

    TextView emptyOrder;
    String idKey;
    ActivityResultLauncher<Intent> mStartForResult = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if(result.getResultCode() == Activity.RESULT_OK){
                        displayOrders();
                    }
                    else{
//                        textView.setText("Ошибка доступа");
                    }
                }
            });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle arguments = getIntent().getExtras();

        assert arguments != null;

        idKey = arguments.get("idKey").toString();

        setContentView(R.layout.activity_order);

//        Objects.requireNonNull(getSupportActionBar()).hide();

        db = FirebaseFirestore.getInstance();

        btOrdersBack = findViewById(R.id.buttonOrderBack);

        emptyOrder = findViewById(R.id.emptyOrder);




        displayOrders();

        onBack();
    }

    //Загружает с Firestore заказы у пользователя и отображает на экране
    private void displayOrders() {
        orders = new ArrayList<>();

        db.collection("users")
                .document(idKey)
                .collection("orders")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@androidx.annotation.NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {

                                Log.d(TAG, document.getId() + " => " + document.getData());
                                Map<String, Object> objectMap = document.getData();

                                orders.add(new OrderModel(
                                                Integer.parseInt(objectMap.get("id").toString()),
                                                objectMap.get("date").toString(),
                                                objectMap.get("address").toString(),
                                                Integer.parseInt(objectMap.get("sum").toString()),
                                                objectMap.get("status").toString(),
                                                Boolean.parseBoolean(objectMap.get("isPaid").toString())

                                        )
                                );
                            }
                            setOrderRecycler(orders);
                        } else {
                            Log.w(TAG, "Error getting documents.", task.getException());
                            Toast.makeText(getApplicationContext(),
                                    "Ошибка!", Toast.LENGTH_SHORT).show();
                        }
                    }

                });

    }

    private void setOrderRecycler(List<OrderModel> orders) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
        orderAdapter = new OrderAdapter(this, orders, new OrderItem() {
            @Override
            public void call(int id, OrderModel model) {
                Intent intent = new Intent(getApplicationContext(), HistoryOfOrdersActivity.class);
                intent.putExtra("id", id);
                intent.putExtra("idKey", idKey);

         intent.putExtra(OrderModel.class.getName(), model);
//                startActivity(intent);
                mStartForResult.launch(intent);
            }

        });

        recyclerOrders = findViewById(R.id.recyclerOrders);

        recyclerOrders.setLayoutManager( layoutManager);

        recyclerOrders.setAdapter(orderAdapter);

        int itemCount = orderAdapter.getItemCount();
        if(itemCount > 0)
            emptyOrder.setVisibility(View.GONE);
        else
            emptyOrder.setVisibility(View.VISIBLE);



    }

    private void onBack() {
        btOrdersBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}