package com.example.furniture_app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.furniture_app.model.UserModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.auth.User;

import java.util.Objects;

public class SignUpActivity extends AppCompatActivity {
    private static final String TAG = "SignUpActivity";

    private FirebaseAuth mAuth;
    TextInputLayout textInputName;

    TextInputLayout textInputEmail;

    TextInputLayout textInputPass;

    TextInputLayout textInputRepPass;

    CheckBox checkBox;

    FirebaseFirestore db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
//        Objects.requireNonNull(getSupportActionBar()).hide();

        textInputName = findViewById(R.id.textInputName);
        textInputEmail = findViewById(R.id.textInputEmail);
        textInputPass = findViewById(R.id.textInputPass);
        textInputRepPass = findViewById(R.id.textInputRepPass);
        checkBox = findViewById(R.id.checkBox);

        db = FirebaseFirestore.getInstance();

//        database = FirebaseDatabase.getInstance("https://furniture-app-d4057-default-rtdb.europe-west1.firebasedatabase.app/");
//        usersDatabaseReference = database.getReference().child("users");
    }

    public void onClickSignIn(View view) {
        Log.d(TAG , "SignIn");
        Intent intent = new Intent(SignUpActivity.this, SignInActivity.class);
        startActivity(intent);
        finish();
    }

    public void onClickSignUp(View view) {

//        Intent intent = new Intent(SignUpActivity.this, HomeActivity.class);
//        startActivity(intent);
//        finish();
        String name = textInputName.getEditText().getText().toString();
        String email = textInputEmail.getEditText().getText().toString();
        String pass = textInputPass.getEditText().getText().toString();
        String repPass = textInputRepPass.getEditText().getText().toString();

        Log.d(TAG , name);
        Log.d(TAG , email);

        checkEditText(name, email, pass, repPass);

    }

    /// Проверка на наличия текста в EditText
    private void checkEditText(String name, String email, String pass, String repeatPass){

        if (name.length() == 0) {
            Toast.makeText(SignUpActivity.this, "Заполните поле ФИО!",
                    Toast.LENGTH_SHORT).show();
            textInputName.getEditText().setError("Заполните поле ФИО!");

        } else if (email.length() == 0) {
            Toast.makeText(SignUpActivity.this, "Заполните поле Email!",
                    Toast.LENGTH_SHORT).show();
            textInputEmail.getEditText() .setError("Заполните поле Email!");
        } else if (pass.length() == 0) {
            Toast.makeText(SignUpActivity.this, "Заполните поле Пароль!",
                    Toast.LENGTH_SHORT).show();
            textInputPass.getEditText().setError("Заполните поле Пароль!");
        }
        else if (repeatPass.length() == 0) {
            Toast.makeText(SignUpActivity.this, "Заполните поле Повторите пароль",
                    Toast.LENGTH_SHORT).show();
            textInputRepPass.getEditText().setError("Заполните поле Повторите пароль");
        } else if (!pass.equals(repeatPass)) {
            Toast.makeText(SignUpActivity.this, "Пароли не совпадают!",
                    Toast.LENGTH_SHORT).show();
            textInputRepPass.getEditText().setError("Пароли не совпадают!");
        } else if (pass.length() < 6){
            Toast.makeText(SignUpActivity.this, "Пароль должен содержать не менее 6 символов",
                    Toast.LENGTH_SHORT).show();
            textInputPass.getEditText().setError("Пароль должен содержать больше 6 символов");
        } else if (checkBox.isChecked() == false){
            Toast.makeText(SignUpActivity.this, "Нажмите на галочку Согласен с Условиями использования",
                    Toast.LENGTH_SHORT).show();
            checkBox.setError("Нажмите на галочку!");
        }
        else {
            mAuth = FirebaseAuth.getInstance();
            createAccount(email,  pass);
        }
    }

    /// Создание учетной записи
    private void createAccount(String email, String pass) {
        // [START create_user_with_email]
        mAuth.createUserWithEmailAndPassword(email, pass)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "createUserWithEmail:success");

                            FirebaseUser user = mAuth.getCurrentUser();
                            createUser(user);

                            Toast.makeText(SignUpActivity.this, "Учетная запись успешно создана!",
                                    Toast.LENGTH_SHORT).show();

                            updateUI(user);

                            sendEmailVerification();

                            Log.d("onClick", "Enter");
                            Intent intent = new Intent(SignUpActivity.this, HomeActivity.class);

                            startActivity(intent);
                            finish();
                        } else {
                            //  If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(SignUpActivity.this, "Не удалось создать учетную запись! " + task.getException().toString(),
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }
                    }
                });
        // [END create_user_with_email]
    }


    /// Создания User в Storage Firebase
    private void createUser(FirebaseUser firebaseUser) {
        UserModel user = new UserModel();

        user.setId(firebaseUser.getUid());
        user.setEmail(firebaseUser.getEmail());
        user.setName(textInputName.getEditText().getText().toString());
        user.setPhoto("");


        ///Отправка в Storage
        db.collection("users")
                .add(user);
//        usersDatabaseReference.push().setValue(user);
    }

    /// Отправка потвержающего письма на почтовый ящик
    private void sendEmailVerification() {
        // Send verification email
        // [START send_email_verification]
        final FirebaseUser user = mAuth.getCurrentUser();
        user.sendEmailVerification()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        // Email sent
                    }
                });
        // [END send_email_verification]
    }

    private void updateUI(FirebaseUser user) {

    }
}