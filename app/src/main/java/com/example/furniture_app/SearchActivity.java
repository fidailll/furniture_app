package com.example.furniture_app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.example.furniture_app.adapter.ProductAdapterGrid;
import com.example.furniture_app.model.ProductModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class SearchActivity extends AppCompatActivity {

    private static final String TAG = "SearchActivity";
    TextInputLayout textInputSearch;
    Button btBack;
    GridView productsGrid;
    List<ProductModel> productModel = new ArrayList<>();
    FirebaseFirestore db;

    ProductAdapterGrid mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
//        Objects.requireNonNull(getSupportActionBar()).hide();

        db = FirebaseFirestore.getInstance();

        btBack = findViewById(R.id.buttonSearchBack);

        textInputSearch = findViewById(R.id.textInputSearch);

        textInputSearch.getBoxStrokeWidthFocused();

        onClickBtBack();

        onEnterInputText();

        productsGrid =  findViewById(R.id.gridSearchProduct);

        productAdapterGrid();


    }

    ///Поиск товара
    void onEnterInputText(){
        textInputSearch.getEditText().setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    String searchData = textInputSearch.getEditText().getText().toString();

                    Log.d(TAG , searchData);

                    List<ProductModel> list = new ArrayList<ProductModel>();

                    for(int i = 0; i < productModel.size(); i++){
                        if (productModel.get(i).getTitle().toLowerCase().contains(searchData.toLowerCase())){
                            list.add(productModel.get(i));
                        }
                    }

                    mAdapter = new ProductAdapterGrid(SearchActivity.this,
                            android.R.layout.simple_list_item_1, list);

//                    productsGrid.setAdapter(null);

                    productsGrid.setAdapter(mAdapter);

                    return true;
                }
                return false;
            }
        });


    }


    void onClickBtBack(){
        btBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    //Выгружает список
    private void productAdapterGrid(){

        db.collection("products").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {

                    for (QueryDocumentSnapshot document : task.getResult()) {

                        Log.d(TAG, document.getId() + " => " + document.getData());
                        Map<String, Object> objectMap = document.getData();
//                        Log.d(TAG, objectMap.get("appRating").toString());

                        productModel.add(new ProductModel(
                                Integer.parseInt(objectMap.get("id").toString()),
                                objectMap.get("title").toString(),
                                objectMap.get("description").toString(),
                                Integer.parseInt(objectMap.get("price").toString()),
                                objectMap.get("srcImage").toString(),
                                Integer.parseInt(objectMap.get("appRating").toString()),
                                objectMap.get("categoryName").toString(),
                                Integer.parseInt(objectMap.get("categoryId").toString()),
                                objectMap.get("manufacturer").toString(),
                                objectMap.get("guarantee").toString(),
                                objectMap.get("lifeTime").toString(),
                                objectMap.get("color").toString(),
                                Integer.parseInt(objectMap.get("length").toString()),
                                Integer.parseInt(objectMap.get("height").toString()),
                                Integer.parseInt(objectMap.get("width").toString())));
                    }

                     mAdapter = new ProductAdapterGrid(SearchActivity.this,
                            android.R.layout.simple_list_item_1, productModel);



                    productsGrid.setAdapter(mAdapter);


                    productsGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View v,
                                                int position, long id) {

                            Intent intent = new Intent(SearchActivity.this, ProductActivity.class);
                            // передаем индекс массива
                            intent. putExtra(ProductModel.class.getName(), mAdapter.getItem(position));

                            startActivity(intent);
                        }
                    });

                } else {
                    Log.w(TAG, "Error getting documents.", task.getException());
                }
            }
        });


    }


}