package com.example.furniture_app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Objects;

public class SignInActivity extends AppCompatActivity {

    private static final String TAG = "SignInActivity";

    TextInputLayout textInputEmail;

    TextInputLayout textInputPass;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
//        Objects.requireNonNull(getSupportActionBar()).hide();

        mAuth = FirebaseAuth.getInstance();


        textInputEmail = findViewById(R.id.textInputEmailIn);
        textInputPass = findViewById(R.id.textInputPassIn);
    }

    public void onClickSignUp(View view) {
        Log.d(TAG , "SignUp");
        Intent intent = new Intent(SignInActivity.this, SignUpActivity.class);
        startActivity(intent);
        finish();
    }


    public void onClickSignIn(View view) {

        String email = textInputEmail.getEditText().getText().toString();
        String pass = textInputPass.getEditText().getText().toString();
        Log.d(TAG , email );
        checkEditText(email, pass);
//        Intent intent = new Intent(SignInActivity.this, HomeActivity.class);
//        startActivity(intent);
//        finish();
    }

    /// Проверка на наличия текста в EditText
    private void checkEditText(String email, String pass){
        if (email.length() == 0) {
            Toast.makeText(SignInActivity.this, "Заполните поле Email!",
                    Toast.LENGTH_SHORT).show();
            textInputEmail.getEditText().setError("Заполните поле Email!");
        } else if (pass.length() == 0) {
            Toast.makeText(SignInActivity.this, "Заполните поле Пароль!",
                    Toast.LENGTH_SHORT).show();
            textInputPass.getEditText().setError("Заполните поле Пароль!");

        } else {
        signInAccount(email, pass);
        }
    }


    /// Войти в аккаунт
    private void signInAccount(String email, String pass) {
        mAuth.signInWithEmailAndPassword(email, pass)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                            Intent intent = new Intent(SignInActivity.this, HomeActivity.class);
                            startActivity(intent);
                            finish();

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(SignInActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            textInputEmail.getEditText().setError("Неправильный логин или пароль!");
                            updateUI(null);
                        }
                    }
                });
    }

    private void reload() { }

    private void updateUI(FirebaseUser user) {

    }

    public void onClickPassRecovery(View view) {
        Intent intent = new Intent(SignInActivity.this, PassRecoveryActivity.class);
        startActivity(intent);
    }
}