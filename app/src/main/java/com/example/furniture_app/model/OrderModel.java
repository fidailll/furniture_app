package com.example.furniture_app.model;

import java.io.Serializable;

public class OrderModel implements Serializable {
    int id;
    String date;
    String address;
    int sum;
    String status;
    boolean isPaid;


    public OrderModel(int id, String date, String address, int sum, String status, boolean isPaid) {
        this.id = id;
        this.date = date;
        this.address = address;
        this.sum = sum;
        this.status = status;
        this.isPaid = isPaid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isPaid() {
        return isPaid;
    }

    public void setPaid(boolean paid) {
        isPaid = paid;
    }
}
