package com.example.furniture_app.model;

import java.io.Serializable;

public class CategoryModel implements Serializable {
    int id;
    String name;
    String srcPhoto;

    public CategoryModel(int id, String name, String srcPhoto) {
        this.id = id;
        this.name = name;
        this.srcPhoto = srcPhoto;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSrcPhoto() {
        return srcPhoto;
    }

    public void setSrcPhoto(String srcPhoto) {
        this.srcPhoto = srcPhoto;
    }

}
