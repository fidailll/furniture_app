package com.example.furniture_app.model;

public class UserProductModel extends ProductModel{
    int count;


    public UserProductModel(int id, String title, String description, int price, String srcImage, int appRating, String categoryName, int categoryId, String manufacturer, String guarantee, String lifeTime, String color, int length, int height, int width, int count) {
        super(id, title, description, price, srcImage, appRating, categoryName, categoryId, manufacturer, guarantee, lifeTime, color, length, height, width);
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
