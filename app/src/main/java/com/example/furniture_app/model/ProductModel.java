package com.example.furniture_app.model;


import java.io.Serializable;

public class ProductModel implements Serializable {
    int id;
    String title;
    String description;
    int price;
    String srcImage;
    int appRating;
    String categoryName;
    int categoryId;
    String manufacturer;
    String guarantee;
    String lifeTime;
    String color;
    int length;
    int height;
    int width;

    public ProductModel(int id, String title, String description, int price, String srcImage, int appRating, String categoryName, int categoryId, String manufacturer, String guarantee, String lifeTime, String color, int length, int height, int width) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.price = price;
        this.srcImage = srcImage;
        this.appRating = appRating;
        this.categoryName = categoryName;
        this.categoryId = categoryId;
        this.manufacturer = manufacturer;
        this.guarantee = guarantee;
        this.lifeTime = lifeTime;
        this.color = color;
        this.length = length;
        this.height = height;
        this.width = width;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getSrcImage() {
        return srcImage;
    }

    public void setSrcImage(String srcImage) {
        this.srcImage = srcImage;
    }

    public int getAppRating() {
        return appRating;
    }

    public void setAppRating(int appRating) {
        this.appRating = appRating;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getGuarantee() {
        return guarantee;
    }

    public void setGuarantee(String guarantee) {
        this.guarantee = guarantee;
    }

    public String getLifeTime() {
        return lifeTime;
    }

    public void setLifeTime(String lifeTime) {
        this.lifeTime = lifeTime;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }
}
